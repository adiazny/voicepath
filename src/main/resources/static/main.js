(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-banner></app-banner>\n<app-dialer (messageEvent)='receiveMessage($event)' (lastCallEvent)='receiveLastCall($event)'></app-dialer>\n<app-results [parentMessage]='message' [lastCall]='lastCallMessage'></app-results>\n<!-- <app-footer></app-footer> -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.receiveMessage = function (message) {
        this.message = message;
        console.log(message);
    };
    AppComponent.prototype.receiveLastCall = function (lastCall) {
        this.lastCallMessage = lastCall;
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _banner_banner_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./banner/banner.component */ "./src/app/banner/banner.component.ts");
/* harmony import */ var _dialer_dialer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dialer/dialer.component */ "./src/app/dialer/dialer.component.ts");
/* harmony import */ var _results_results_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./results/results.component */ "./src/app/results/results.component.ts");
/* harmony import */ var _call_results_table_call_results_table_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./call-results-table/call-results-table.component */ "./src/app/call-results-table/call-results-table.component.ts");
/* harmony import */ var _call_result_call_result_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./call-result/call-result.component */ "./src/app/call-result/call-result.component.ts");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @stomp/ng2-stompjs */ "./node_modules/@stomp/ng2-stompjs/fesm5/stomp-ng2-stompjs.js");
/* harmony import */ var _stomp_config__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./stomp.config */ "./src/app/stomp.config.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _map_map_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./map/map.component */ "./src/app/map/map.component.ts");
/* harmony import */ var _services_map_map_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/map/map.service */ "./src/app/services/map/map.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














 // <-- NgModel lives here






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _banner_banner_component__WEBPACK_IMPORTED_MODULE_4__["BannerComponent"],
                _dialer_dialer_component__WEBPACK_IMPORTED_MODULE_5__["DialerComponent"],
                _results_results_component__WEBPACK_IMPORTED_MODULE_6__["ResultsComponent"],
                _call_results_table_call_results_table_component__WEBPACK_IMPORTED_MODULE_7__["CallResultsTableComponent"],
                _call_result_call_result_component__WEBPACK_IMPORTED_MODULE_8__["CallResultComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_18__["FooterComponent"],
                _map_map_component__WEBPACK_IMPORTED_MODULE_19__["MapComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_9__["MatToolbarModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_13__["MatTableModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HttpClientModule"],
            ],
            providers: [_services_map_map_service__WEBPACK_IMPORTED_MODULE_20__["MapService"],
                {
                    provide: _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_16__["InjectableRxStompConfig"],
                    useValue: _stomp_config__WEBPACK_IMPORTED_MODULE_17__["stompConfig"]
                },
                {
                    provide: _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_16__["RxStompService"],
                    useFactory: _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_16__["rxStompServiceFactory"],
                    deps: [_stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_16__["InjectableRxStompConfig"]]
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/banner/banner.component.css":
/*!*********************************************!*\
  !*** ./src/app/banner/banner.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/banner/banner.component.html":
/*!**********************************************!*\
  !*** ./src/app/banner/banner.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p>\n  Banner works!\n</p> -->\n<mat-toolbar color=\"primary\">\n  <mat-toolbar-row>\n    <span>Voice Path</span>\n  </mat-toolbar-row>\n</mat-toolbar>\n"

/***/ }),

/***/ "./src/app/banner/banner.component.ts":
/*!********************************************!*\
  !*** ./src/app/banner/banner.component.ts ***!
  \********************************************/
/*! exports provided: BannerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerComponent", function() { return BannerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BannerComponent = /** @class */ (function () {
    function BannerComponent() {
    }
    BannerComponent.prototype.ngOnInit = function () {
    };
    BannerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-banner',
            template: __webpack_require__(/*! ./banner.component.html */ "./src/app/banner/banner.component.html"),
            styles: [__webpack_require__(/*! ./banner.component.css */ "./src/app/banner/banner.component.css")],
        }),
        __metadata("design:paramtypes", [])
    ], BannerComponent);
    return BannerComponent;
}());



/***/ }),

/***/ "./src/app/call-result/call-result.component.css":
/*!*******************************************************!*\
  !*** ./src/app/call-result/call-result.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container{\n  width:100%;\n}\n\n/* Column Widths */\n\n.mat-column-fromNumber,\n.mat-column-toNumber,\n.mat-column-callStatus,\n.mat-column-startTime,\n.mat-column-endTime{\n  max-width: 128px;\n}"

/***/ }),

/***/ "./src/app/call-result/call-result.component.html":
/*!********************************************************!*\
  !*** ./src/app/call-result/call-result.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"data\">\n\n    <!--- Note that these columns can be defined in any order.\n          The actual rendered columns are set as a property on the row definition\" -->\n\n    <!-- Position Column -->\n    <ng-container matColumnDef=\"twilioSid\">\n      <mat-header-cell *matHeaderCellDef> Twilio SID </mat-header-cell>\n      <mat-cell *matCellDef=\"let call\"> {{call.twilioSid}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"toNumber\">\n      <mat-header-cell *matHeaderCellDef> To Number </mat-header-cell>\n      <mat-cell *matCellDef=\"let call\"> {{call.toNumber}} </mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"fromNumber\">\n      <mat-header-cell *matHeaderCellDef> From Number </mat-header-cell>\n      <mat-cell *matCellDef=\"let call\"> {{call.fromNumber}} </mat-cell>\n    </ng-container>\n\n    <!-- Weight Column -->\n    <ng-container matColumnDef=\"callStatus\">\n      <mat-header-cell *matHeaderCellDef> Call Status </mat-header-cell>\n      <mat-cell *matCellDef=\"let call\"> {{call.callStatus}} </mat-cell>\n    </ng-container>\n\n    <!-- Symbol Column -->\n    <ng-container matColumnDef=\"startTime\">\n      <mat-header-cell *matHeaderCellDef> Start Time </mat-header-cell>\n      <mat-cell *matCellDef=\"let call\"> {{call.startTime}} </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"endTime\">\n      <mat-header-cell *matHeaderCellDef> End Time </mat-header-cell>\n      <mat-cell *matCellDef=\"let call\"> {{call.endTime}} </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n</div>\n"

/***/ }),

/***/ "./src/app/call-result/call-result.component.ts":
/*!******************************************************!*\
  !*** ./src/app/call-result/call-result.component.ts ***!
  \******************************************************/
/*! exports provided: CallResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallResultComponent", function() { return CallResultComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CallResultComponent = /** @class */ (function () {
    function CallResultComponent() {
        this.displayedColumns = ['twilioSid', 'toNumber', 'fromNumber', 'callStatus', 'startTime', 'endTime'];
    }
    CallResultComponent.prototype.ngOnChanges = function (changes) {
        this.data = this.resultMessage;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('table'),
        __metadata("design:type", _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatTable"])
    ], CallResultComponent.prototype, "table", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], CallResultComponent.prototype, "resultMessage", void 0);
    CallResultComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-call-result',
            template: __webpack_require__(/*! ./call-result.component.html */ "./src/app/call-result/call-result.component.html"),
            styles: [__webpack_require__(/*! ./call-result.component.css */ "./src/app/call-result/call-result.component.css")]
        })
    ], CallResultComponent);
    return CallResultComponent;
}());



/***/ }),

/***/ "./src/app/call-results-table/call-results-table.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/call-results-table/call-results-table.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/call-results-table/call-results-table.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/call-results-table/call-results-table.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  call-results-table works!\n</p>\n"

/***/ }),

/***/ "./src/app/call-results-table/call-results-table.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/call-results-table/call-results-table.component.ts ***!
  \********************************************************************/
/*! exports provided: CallResultsTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallResultsTableComponent", function() { return CallResultsTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CallResultsTableComponent = /** @class */ (function () {
    function CallResultsTableComponent() {
    }
    CallResultsTableComponent.prototype.ngOnInit = function () {
    };
    CallResultsTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-call-results-table',
            template: __webpack_require__(/*! ./call-results-table.component.html */ "./src/app/call-results-table/call-results-table.component.html"),
            styles: [__webpack_require__(/*! ./call-results-table.component.css */ "./src/app/call-results-table/call-results-table.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CallResultsTableComponent);
    return CallResultsTableComponent;
}());



/***/ }),

/***/ "./src/app/dialer/dialer.component.css":
/*!*********************************************!*\
  !*** ./src/app/dialer/dialer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .example-container {\n  display: flex;\n  flex-direction: row;\n} */\n\n.example-container > * {\n  padding: 0px 8px 0px 8px;\n  margin-top: 24px;\n}\n\n/* .example-container > input {\n  width: 25%;\n}  */\n\n.example-container > button {\n  line-height: 30px;\n}\n\n.wide-box {\n  width: 80%;\n}\n"

/***/ }),

/***/ "./src/app/dialer/dialer.component.html":
/*!**********************************************!*\
  !*** ./src/app/dialer/dialer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\n  <mat-form-field>\n    <input matInput placeholder=\"+19171234567\" type='tel' [(ngModel)]=\"called_number\">\n  </mat-form-field>\n  <button mat-raised-button color=\"primary\" (click)=\"dial(called_number)\">Dial</button>\n</div>\n"

/***/ }),

/***/ "./src/app/dialer/dialer.component.ts":
/*!********************************************!*\
  !*** ./src/app/dialer/dialer.component.ts ***!
  \********************************************/
/*! exports provided: DialerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialerComponent", function() { return DialerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_dialer_dialer_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/dialer/dialer.service */ "./src/app/services/dialer/dialer.service.ts");
/* harmony import */ var _models_call__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/call */ "./src/app/models/call.ts");
/* harmony import */ var _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @stomp/ng2-stompjs */ "./node_modules/@stomp/ng2-stompjs/fesm5/stomp-ng2-stompjs.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DialerComponent = /** @class */ (function () {
    function DialerComponent(dialerService, rxStompService) {
        this.dialerService = dialerService;
        this.rxStompService = rxStompService;
        this.call = new _models_call__WEBPACK_IMPORTED_MODULE_2__["Call"]();
        this.calling_number = "555-555-5555";
        this.called_number = "";
        this.regex = new RegExp("^\\+1\\d{10}$");
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"]([]);
        this.callData = [];
        this.messageEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.lastCallEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    DialerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rxStompService.watch('/topic/clientcalls').subscribe(function (message) {
            var obj = JSON.parse(message.body);
            var callDataToAdd = new _models_call__WEBPACK_IMPORTED_MODULE_2__["Call"]();
            callDataToAdd.twilioSid = obj.twilioSid;
            callDataToAdd.callStatus = obj.callStatus;
            callDataToAdd.direction = obj.direction;
            callDataToAdd.endTime = obj.endTime;
            callDataToAdd.fromNumber = obj.fromNumber;
            callDataToAdd.startTime = obj.startTime;
            callDataToAdd.toNumber = obj.toNumber;
            _this.callData.push(callDataToAdd);
            _this.lastCallEvent.emit(callDataToAdd);
            _this.subject.next(_this.callData);
        });
    };
    DialerComponent.prototype.dial = function (numbers) {
        var _this = this;
        var numberArray = numbers.split(',');
        numberArray.forEach(function (number) {
            _this.validFormat = _this.regex.test(number);
            if (!_this.validFormat) {
                alert("Invalid Number");
                return;
            }
            _this.dialerService.call(number).subscribe(function (foo) {
                console.log(foo);
                _this.messageEvent.emit(_this.subject);
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DialerComponent.prototype, "messageEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DialerComponent.prototype, "lastCallEvent", void 0);
    DialerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dialer',
            template: __webpack_require__(/*! ./dialer.component.html */ "./src/app/dialer/dialer.component.html"),
            styles: [__webpack_require__(/*! ./dialer.component.css */ "./src/app/dialer/dialer.component.css")]
        }),
        __metadata("design:paramtypes", [_services_dialer_dialer_service__WEBPACK_IMPORTED_MODULE_1__["DialerService"], _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_3__["RxStompService"]])
    ], DialerComponent);
    return DialerComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#footer {\n  position: absolute;\n  bottom:  0;\n  width: 100%;\n  height: 60px;\n  font-size: 13px;\n}\n\n.example-spacer {\n  flex: 1 1 auto;\n}\n\n.IM-logo {\n  background-image: url('95-953266_iron-man-svg-download-iron-man-svg-black.png');\n  background-position: center center;\n  float: right;\n  height: 30px;\n  margin-bottom: 9.5px;\n  margin-right: 20px;\n  margin-top: 9.5px;\n  width: 30px;\n  size: 30px;\n}"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar id='footer' color=\"primary\">\n  <mat-toolbar-row>\n    <span>\n      <p>Copyright © 2019 Adeel Khan & Co. All rights reserved.</p>\n    </span>\n    <span class=\"example-spacer\"></span>\n  </mat-toolbar-row>\n</mat-toolbar>\n"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/map/map.component.css":
/*!***************************************!*\
  !*** ./src/app/map/map.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/map/map.component.html":
/*!****************************************!*\
  !*** ./src/app/map/map.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"map\" id=\"map\"></div>"

/***/ }),

/***/ "./src/app/map/map.component.ts":
/*!**************************************!*\
  !*** ./src/app/map/map.component.ts ***!
  \**************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_map_map_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/map/map.service */ "./src/app/services/map/map.service.ts");
/* harmony import */ var mapbox_gl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! mapbox-gl */ "./node_modules/mapbox-gl/dist/mapbox-gl.js");
/* harmony import */ var mapbox_gl__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mapbox_gl__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _models_call__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/call */ "./src/app/models/call.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MapComponent = /** @class */ (function () {
    function MapComponent(mapService) {
        this.mapService = mapService;
        this.style = 'mapbox://styles/mapbox/dark-v9';
        this.lat = 40;
        this.lng = -74.50;
        this.message = 'Hello World!';
        this.numberGeoMap = [{
                number: '+19175617031',
                coordinates: [
                    -73.6406,
                    40.7492
                ],
                latestCallStatus: 'initiated'
            },
            {
                number: '+12015958494',
                coordinates: [
                    -74.6593,
                    40.349
                ],
                latestCallStatus: 'initiated'
            }, {
                number: '+15005550001',
                coordinates: [
                    -71.412834,
                    41.8239
                ],
                latestCallStatus: 'initiated'
            }, {
                number: '+16318384707',
                coordinates: [
                    -79.9754,
                    40.4406
                ],
                latestCallStatus: 'initiated'
            }
        ];
        this.markers = {};
    }
    MapComponent.prototype.ngOnChanges = function () {
        var _this = this;
        console.log("Map on changes");
        if (this.lastCall) {
            this.numberGeoMap.forEach(function (mapping) {
                if (_this.lastCall.toNumber === mapping.number) {
                    mapping.latestCallStatus = _this.lastCall.callStatus;
                    // this.marker.remove();
                    var markerToUpdate = _this.markers[_this.lastCall.toNumber];
                    markerToUpdate.remove();
                    _this.markers[_this.lastCall.toNumber] = new mapbox_gl__WEBPACK_IMPORTED_MODULE_2__["Marker"]({ color: mapping.latestCallStatus === 'initiated' ? '#D3D3D3' : mapping.latestCallStatus === 'failed' ? '#FF0000' : mapping.latestCallStatus === 'completed' ? '#32CD32' : '#FFFF33' })
                        .setLngLat(mapping.coordinates)
                        .addTo(_this.map);
                }
            });
        }
    };
    MapComponent.prototype.ngOnInit = function () {
        // this.markers = this.mapService.getMarkers()
        this.buildMap();
    };
    MapComponent.prototype.buildMap = function () {
        var _this = this;
        this.map = new mapbox_gl__WEBPACK_IMPORTED_MODULE_2__["Map"]({
            container: 'map',
            style: this.style,
            zoom: 5,
            center: [this.lng, this.lat]
        });
        this.map.on('load', function () {
            _this.numberGeoMap.forEach(function (element) {
                _this.markers[element.number] = new mapbox_gl__WEBPACK_IMPORTED_MODULE_2__["Marker"]({ color: element.latestCallStatus === 'initiated' ? '#D3D3D3' : element.latestCallStatus === 'failed' ? '#FF0000' : element.latestCallStatus === 'completed' ? '#32CD32' : '#FFFF33' })
                    .setLngLat(element.coordinates)
                    .addTo(_this.map);
            });
            // })
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _models_call__WEBPACK_IMPORTED_MODULE_3__["Call"])
    ], MapComponent.prototype, "lastCall", void 0);
    MapComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-map',
            template: __webpack_require__(/*! ./map.component.html */ "./src/app/map/map.component.html"),
            styles: [__webpack_require__(/*! ./map.component.css */ "./src/app/map/map.component.css")]
        }),
        __metadata("design:paramtypes", [_services_map_map_service__WEBPACK_IMPORTED_MODULE_1__["MapService"]])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/models/call.ts":
/*!********************************!*\
  !*** ./src/app/models/call.ts ***!
  \********************************/
/*! exports provided: Call */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Call", function() { return Call; });
var Call = /** @class */ (function () {
    function Call() {
    }
    return Call;
}());



/***/ }),

/***/ "./src/app/results/results.component.css":
/*!***********************************************!*\
  !*** ./src/app/results/results.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".result-container {\n  display: inline-flex;\n}"

/***/ }),

/***/ "./src/app/results/results.component.html":
/*!************************************************!*\
  !*** ./src/app/results/results.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='result-container'>\n  <app-map [lastCall]='lastCall'></app-map>\n  <app-call-result *ngIf=\"parentMessage\" [resultMessage]='parentMessage'></app-call-result>\n</div>"

/***/ }),

/***/ "./src/app/results/results.component.ts":
/*!**********************************************!*\
  !*** ./src/app/results/results.component.ts ***!
  \**********************************************/
/*! exports provided: ResultsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsComponent", function() { return ResultsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_call__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/call */ "./src/app/models/call.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResultsComponent = /** @class */ (function () {
    function ResultsComponent() {
    }
    ResultsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], ResultsComponent.prototype, "parentMessage", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _models_call__WEBPACK_IMPORTED_MODULE_1__["Call"])
    ], ResultsComponent.prototype, "lastCall", void 0);
    ResultsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-results',
            template: __webpack_require__(/*! ./results.component.html */ "./src/app/results/results.component.html"),
            styles: [__webpack_require__(/*! ./results.component.css */ "./src/app/results/results.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ResultsComponent);
    return ResultsComponent;
}());



/***/ }),

/***/ "./src/app/services/dialer/dialer.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/dialer/dialer.service.ts ***!
  \***************************************************/
/*! exports provided: DialerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialerService", function() { return DialerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DialerService = /** @class */ (function () {
    // path: string = 'http://voicepath.us-east-1.elasticbeanstalk.com/makecall'
    function DialerService(httpClient) {
        this.httpClient = httpClient;
        this.path = "/makecall";
    }
    DialerService.prototype.call = function (number) {
        // this.httpClient.post(this.url, call_data) // Make Http Post to initiate call
        var url = new URL(this.path, window.location.href);
        return this.httpClient.post(url.href + ("/" + number), {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (val) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])("Got the following error " + val); }));
    };
    DialerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DialerService);
    return DialerService;
}());



/***/ }),

/***/ "./src/app/services/map/map.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/map/map.service.ts ***!
  \*********************************************/
/*! exports provided: MapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapService", function() { return MapService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var mapbox_gl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! mapbox-gl */ "./node_modules/mapbox-gl/dist/mapbox-gl.js");
/* harmony import */ var mapbox_gl__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mapbox_gl__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';


var MapService = /** @class */ (function () {
    function MapService(http) {
        this.http = http;
        mapbox_gl__WEBPACK_IMPORTED_MODULE_2__["accessToken"] = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].mapbox.accessToken;
    }
    MapService.prototype.getMarkers = function () {
        return this.http.get('src/app/services/map/map.geo.json');
    };
    MapService.prototype.createMarker = function (data) {
        // return this.db.list('/markers')
        //               .push(data)
    };
    MapService.prototype.removeMarker = function ($key) {
        // return this.db.object('/markers/' + $key).remove()
    };
    MapService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], MapService);
    return MapService;
}());



/***/ }),

/***/ "./src/app/stomp.config.ts":
/*!*********************************!*\
  !*** ./src/app/stomp.config.ts ***!
  \*********************************/
/*! exports provided: stompConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stompConfig", function() { return stompConfig; });
var prepareBrokerURL = function (path) {
    // Create a relative http(s) URL relative to current page
    var url = new URL(path, window.location.href);
    // Convert protocol http -> ws and https -> wss
    url.protocol = url.protocol.replace('http', 'ws');
    return url.href;
};
var stompConfig = {
    brokerURL: prepareBrokerURL('/voicepath'),
    // brokerURL: 'ws://voicepath.us-east-1.elasticbeanstalk.com/voicepath',
    heartbeatIncoming: 0,
    heartbeatOutgoing: 0,
    reconnectDelay: 200,
    debug: function (msg) {
        console.log(new Date(), msg);
    }
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    mapbox: {
        accessToken: 'pk.eyJ1IjoiamNhc3Q2MjMiLCJhIjoiY2p4ZHl5d2EzMGpmOTNwbzlzdGhtazMxbSJ9.19dUTPi0bOFPJDY9Ieh30Q'
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/juliocastillo/hackathon/voicepath/src/main/web/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map