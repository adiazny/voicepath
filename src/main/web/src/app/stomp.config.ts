import { InjectableRxStompConfig } from "@stomp/ng2-stompjs";

const prepareBrokerURL = (path: string): string => {
  // Create a relative http(s) URL relative to current page
  const url = new URL(path, window.location.href);
  // Convert protocol http -> ws and https -> wss
  url.protocol = url.protocol.replace('http', 'ws');

  return url.href;
};

export const stompConfig: InjectableRxStompConfig = {
  brokerURL: prepareBrokerURL('/voicepath'),
  // brokerURL: 'ws://voicepath.us-east-1.elasticbeanstalk.com/voicepath',

  heartbeatIncoming: 0,
  heartbeatOutgoing: 0,

  reconnectDelay: 200,

  debug: (msg: string): void => {
    console.log(new Date(), msg);
  }

};
