import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallResultsTableComponent } from './call-results-table.component';

describe('CallResultsTableComponent', () => {
  let component: CallResultsTableComponent;
  let fixture: ComponentFixture<CallResultsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallResultsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallResultsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
