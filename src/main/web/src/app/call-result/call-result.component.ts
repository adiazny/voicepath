import { Component, Input, OnChanges, ViewChild, SimpleChanges } from '@angular/core';
import { Call } from '../models/call'
import { MatTable } from '@angular/material/table';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-call-result',
  templateUrl: './call-result.component.html',
  styleUrls: ['./call-result.component.css']
})
export class CallResultComponent implements OnChanges {
  @ViewChild('table') table: MatTable<Call>;
  @Input() resultMessage: Observable<Call[]>;
  data : Observable<Call[]>;

  displayedColumns = ['twilioSid', 'toNumber', 'fromNumber', 'callStatus', 'startTime', 'endTime'];

  ngOnChanges(changes : SimpleChanges)	{
    this.data = this.resultMessage;
  }
}



  
