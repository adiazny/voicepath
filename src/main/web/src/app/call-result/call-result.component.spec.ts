import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallResultComponent } from './call-result.component';

describe('CallResultComponent', () => {
  let component: CallResultComponent;
  let fixture: ComponentFixture<CallResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
