import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DialerService } from '../services/dialer/dialer.service';
import { Call } from '../models/call'
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from "@stomp/stompjs";
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-dialer',
  templateUrl: './dialer.component.html',
  styleUrls: ['./dialer.component.css']
})
export class DialerComponent implements OnInit {
  call: Call = new Call();
  calling_number: string = "555-555-5555"
  called_number: string = ""
  validFormat: boolean;
  regex: RegExp = new RegExp("^\\+1\\d{10}$");
  subject: BehaviorSubject<Call[]> = new BehaviorSubject<Call[]>([]);
  callData: Call[] = [];

  @Output() messageEvent = new EventEmitter<Observable<Call[]>>();
  @Output() lastCallEvent = new EventEmitter<Call>();
  constructor(private dialerService: DialerService, private rxStompService: RxStompService) { }

  ngOnInit() {

    this.rxStompService.watch('/topic/clientcalls').subscribe((message: Message) => {
      var obj = JSON.parse(message.body);
      let callDataToAdd = new Call();
      callDataToAdd.twilioSid = obj.twilioSid
      callDataToAdd.callStatus = obj.callStatus
      callDataToAdd.direction = obj.direction
      callDataToAdd.endTime = obj.endTime
      callDataToAdd.fromNumber = obj.fromNumber
      callDataToAdd.startTime = obj.startTime
      callDataToAdd.toNumber = obj.toNumber
      this.callData.push(callDataToAdd)
      this.lastCallEvent.emit(callDataToAdd);
      this.subject.next(this.callData)
    })
  }

  dial(numbers: string) {
    let numberArray = numbers.split(',');

    numberArray.forEach(number => {
      this.validFormat = this.regex.test(number);
      if (!this.validFormat) {
        alert("Invalid Number")
        return
      }

      this.dialerService.call(number).subscribe(foo => {
        console.log(foo);
        this.messageEvent.emit(this.subject);
      });
    });
  }



}
