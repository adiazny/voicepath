import { Component, OnInit, Input, Output } from '@angular/core';
import { Call } from '../models/call';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  @Input() parentMessage : Observable<Call[]>;
  @Input() lastCall : Call;

  constructor() { }

  ngOnInit() {
  }

}
