import { Component } from '@angular/core';
import { Call } from './models/call';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  message : Observable<Call[]>;
  lastCallMessage: Call;

  receiveMessage(message: Observable<Call[]>){
    this.message = message;
    console.log(message)
  }

  receiveLastCall(lastCall : Call){
    this.lastCallMessage = lastCall;
  }
}



