import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MapService } from '../services/map/map.service';
import * as mapboxgl from 'mapbox-gl';
import { Observable } from 'rxjs';
import { Call } from '../models/call';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnChanges {

  /// default settings
  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/dark-v9';
  lat = 40;
  lng = -74.50;
  message = 'Hello World!';
  @Input() lastCall: Call;
  numberGeoMap = [{
    number: '+12015951700',
    coordinates: [
      -73.6406,
      40.7492
    ],
    latestCallStatus: 'initiated'
  },
  {
    number: '+12012735002',
    coordinates: [
      -74.6593,
      40.349
    ],
    latestCallStatus: 'initiated'
  }, {
    number: '+15512053287',
    coordinates: [
      -71.412834,
      41.8239
    ],
    latestCallStatus: 'initiated'
  }, {
    number: '+15512053146',
    coordinates: [
      -79.9754,
     40.4406
  ],
    latestCallStatus: 'initiated'
  }
  ]
  // data
  source: Call[];
  markers = {};

  marker: mapboxgl.Marker;

  constructor(private mapService: MapService) {
  }

  ngOnChanges() {
    console.log("Map on changes")
    if (this.lastCall) {
      this.numberGeoMap.forEach(mapping => {
        if (this.lastCall.toNumber === mapping.number) {
          mapping.latestCallStatus = this.lastCall.callStatus
          // this.marker.remove();
          let markerToUpdate = this.markers[this.lastCall.toNumber]
          markerToUpdate.remove();
          this.markers[this.lastCall.toNumber] = new mapboxgl.Marker({ color: mapping.latestCallStatus === 'initiated' ? '#D3D3D3' : mapping.latestCallStatus === 'failed' ? '#FF0000' : mapping.latestCallStatus === 'completed' ? '#32CD32' : '#FFFF33' })
            .setLngLat(mapping.coordinates)
            .addTo(this.map);
        }
      })
    }
  }


  ngOnInit() {
    // this.markers = this.mapService.getMarkers()
    this.buildMap()
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 5,
      center: [this.lng, this.lat]
    });

    this.map.on('load', () => {
      this.numberGeoMap.forEach(element => {
        this.markers[element.number] = new mapboxgl.Marker({ color: element.latestCallStatus === 'initiated' ? '#D3D3D3' : element.latestCallStatus === 'failed' ? '#FF0000' : element.latestCallStatus === 'completed' ? '#32CD32' : '#FFFF33' })
          .setLngLat(element.coordinates)
          .addTo(this.map);
      });
      // })
    })
  }
}