import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'
// import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as mapboxgl from 'mapbox-gl';
import { GeoJson } from 'src/app/models/map';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MapService {

  constructor(private http: HttpClient) {
    mapboxgl.accessToken = environment.mapbox.accessToken
  }


  getMarkers(): Observable<any> {
    return this.http.get('src/app/services/map/map.geo.json');
  }

  createMarker(data: GeoJson) {
    // return this.db.list('/markers')
    //               .push(data)
  }

  removeMarker($key: string) {
    // return this.db.object('/markers/' + $key).remove()
  }

}