import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Call } from '../../models/call'
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class DialerService {
  path: string = "/makecall"
  // path: string = 'http://voicepath.us-east-1.elasticbeanstalk.com/makecall'
  constructor(private httpClient: HttpClient) { }

  call(number: string): Observable<any> {
    // this.httpClient.post(this.url, call_data) // Make Http Post to initiate call
    const url = new URL(this.path, window.location.href);
    return this.httpClient.post<any>(url.href + `/${number}`, {})
      .pipe(
        catchError(val => of(`Got the following error ${val}`))
      );
  }
}
