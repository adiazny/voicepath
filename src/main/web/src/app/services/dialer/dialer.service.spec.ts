import { TestBed } from '@angular/core/testing';

import { DialerService } from './dialer.service';

describe('DailerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DialerService = TestBed.get(DialerService);
    expect(service).toBeTruthy();
  });
});
