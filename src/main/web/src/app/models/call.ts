export class Call{
    _id: string;
    twilioSid: string;
    toNumber: string;
    fromNumber: string;
    callStatus: string;
    startTime: string;
    endTime: string;
    direction: string;
}