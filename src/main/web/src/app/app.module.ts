import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { BannerComponent } from './banner/banner.component';
import { DialerComponent } from './dialer/dialer.component';
import { ResultsComponent } from './results/results.component';
import { CallResultsTableComponent } from './call-results-table/call-results-table.component';
import { CallResultComponent } from './call-result/call-result.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import { MatTableModule} from '@angular/material/table'

import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { stompConfig } from './stomp.config';
import { FooterComponent } from './footer/footer.component';
import { MapComponent } from './map/map.component';

import { MapService } from './services/map/map.service';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    DialerComponent,
    ResultsComponent,
    CallResultsTableComponent,
    CallResultComponent,
    FooterComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
  ],
  providers: [MapService,
    {
      provide: InjectableRxStompConfig,
      useValue: stompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
