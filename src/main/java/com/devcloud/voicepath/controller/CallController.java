package com.devcloud.voicepath.controller;

import com.devcloud.voicepath.model.VoicePathCall;
import com.devcloud.voicepath.service.CallService;
import com.twilio.rest.api.v2010.account.Call;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.awt.*;


@Controller
public class CallController {

    private CallService callService;

    public CallController(CallService callService){
        this.callService = callService;
    }

    @RequestMapping(value = "/makecall/{toNumber}", method = RequestMethod.POST)
    @ResponseBody
    public VoicePathCall makeCall(@PathVariable String toNumber){

        Call call = callService.makeCall(toNumber);
        VoicePathCall voicePathCall = callService.twilioCallToVoicePathCall(call);
        callService.saveVoicePathCall(voicePathCall);

        return voicePathCall;

    }

}
