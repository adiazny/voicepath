package com.devcloud.voicepath.controller;

import com.devcloud.voicepath.model.TwilioCall;
import com.devcloud.voicepath.model.VoicePathCall;
import com.devcloud.voicepath.repository.VoicePathCallDAO;
import com.devcloud.voicepath.service.CallService;
import com.devcloud.voicepath.service.EmailService;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class WebHookController {


    @Autowired
    CallService callService;

    @Autowired
    EmailService emailService;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @RequestMapping(value = "/webhook", method = RequestMethod.POST)
    @ResponseBody
    public String postTwilioWebHook(TwilioCall twilioCall) throws IOException {

        VoicePathCall voicePathCall = callService.twilioCallWebHookToVoicePathCall(twilioCall);
        VoicePathCall updatedVoicePathCall = callService.updateVoicePathCall(voicePathCall);
        if (voicePathCall.getCallStatus().equalsIgnoreCase("Failed")|| voicePathCall.getCallStatus().equalsIgnoreCase("Busy")) {
           updatedVoicePathCall.setCallStatus("failed");
            callService.sendMessage("+18622079148");
            callService.makeCallForFailure("+18624859044");
            this.simpMessagingTemplate.convertAndSend("/topic/clientcalls", updatedVoicePathCall);
            try {
                emailService.sendEmailAlert("tech@voicepath.com", "awad.shaden@gmail.com",
                        "ALERT: VoicePath Issue", "A failure has been detected in the following " +
                                "phone number: " + voicePathCall.getToNumber() + System.lineSeparator() + "The phone number belongs to " +
                                "the Princeton site.");
            } catch (IOException ex) {
                throw ex;
            }
        }else {
            this.simpMessagingTemplate.convertAndSend("/topic/clientcalls", updatedVoicePathCall);
        }
        return "Completed";
    }
}
