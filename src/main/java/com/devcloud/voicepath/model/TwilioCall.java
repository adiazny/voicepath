package com.devcloud.voicepath.model;

public class TwilioCall {

    private String accountSid;
    private String callSid;
    private String callStatus;
    private String callbackSource;
    private String called;
    private String calledCity;
    private String calledCountry;
    private String calledState;
    private String calledZip;
    private String caller;
    private String callerCity;
    private String callerCountry;
    private String callerState;
    private String callerZip;
    private String direction;
    private String from;
    private String fromCity;
    private String fromCountry;
    private String fromState;
    private String fromZip;
    private String sequenceNumber;
    private String timestamp;
    private String to;
    private String toCity;
    private String toCountry;
    private String toState;
    private String toZip;

    public TwilioCall() {
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getCallSid() {
        return callSid;
    }

    public void setCallSid(String callSid) {
        this.callSid = callSid;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public String getCallbackSource() {
        return callbackSource;
    }

    public void setCallbackSource(String callbackSource) {
        this.callbackSource = callbackSource;
    }

    public String getCalled() {
        return called;
    }

    public void setCalled(String called) {
        this.called = called;
    }

    public String getCalledCity() {
        return calledCity;
    }

    public void setCalledCity(String calledCity) {
        this.calledCity = calledCity;
    }

    public String getCalledCountry() {
        return calledCountry;
    }

    public void setCalledCountry(String calledCountry) {
        this.calledCountry = calledCountry;
    }

    public String getCalledState() {
        return calledState;
    }

    public void setCalledState(String calledState) {
        this.calledState = calledState;
    }

    public String getCalledZip() {
        return calledZip;
    }

    public void setCalledZip(String calledZip) {
        this.calledZip = calledZip;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getCallerCity() {
        return callerCity;
    }

    public void setCallerCity(String callerCity) {
        this.callerCity = callerCity;
    }

    public String getCallerCountry() {
        return callerCountry;
    }

    public void setCallerCountry(String callerCountry) {
        this.callerCountry = callerCountry;
    }

    public String getCallerState() {
        return callerState;
    }

    public void setCallerState(String callerState) {
        this.callerState = callerState;
    }

    public String getCallerZip() {
        return callerZip;
    }

    public void setCallerZip(String callerZip) {
        this.callerZip = callerZip;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getFromZip() {
        return fromZip;
    }

    public void setFromZip(String fromZip) {
        this.fromZip = fromZip;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    public String getToZip() {
        return toZip;
    }

    public void setToZip(String toZip) {
        this.toZip = toZip;
    }

    @Override
    public String toString() {
        return "TwilioCallEvent{" +
                "accountSid='" + accountSid + '\'' +
                ", callSid='" + callSid + '\'' +
                ", callStatus='" + callStatus + '\'' +
                ", callbackSource='" + callbackSource + '\'' +
                ", called='" + called + '\'' +
                ", calledCity='" + calledCity + '\'' +
                ", calledCountry='" + calledCountry + '\'' +
                ", calledState='" + calledState + '\'' +
                ", calledZip='" + calledZip + '\'' +
                ", caller='" + caller + '\'' +
                ", callerCity='" + callerCity + '\'' +
                ", callerCountry='" + callerCountry + '\'' +
                ", callerState='" + callerState + '\'' +
                ", callerZip='" + callerZip + '\'' +
                ", direction='" + direction + '\'' +
                ", from='" + from + '\'' +
                ", fromCity='" + fromCity + '\'' +
                ", fromCountry='" + fromCountry + '\'' +
                ", fromState='" + fromState + '\'' +
                ", fromZip='" + fromZip + '\'' +
                ", sequenceNumber='" + sequenceNumber + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", to='" + to + '\'' +
                ", toCity='" + toCity + '\'' +
                ", toCountry='" + toCountry + '\'' +
                ", toState='" + toState + '\'' +
                ", toZip='" + toZip + '\'' +
                '}';
    }


}
