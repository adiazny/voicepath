package com.devcloud.voicepath.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VoicePathCall {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String twilioSid;
    private String toNumber;
    private String fromNumber;
    private String callStatus;
    private String startTime;
    private String endTime;
    private String direction;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTwilioSid() {
        return twilioSid;
    }

    public void setTwilioSid(String twilioSid) {
        this.twilioSid = twilioSid;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "VoicePathCall{" +
                "id=" + id +
                ", twilioSid='" + twilioSid + '\'' +
                ", toNumber='" + toNumber + '\'' +
                ", fromNumber='" + fromNumber + '\'' +
                ", callStatus='" + callStatus + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", direction='" + direction + '\'' +
                '}';
    }//toString
}//class
