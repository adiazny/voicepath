package com.devcloud.voicepath;

import com.devcloud.voicepath.controller.CallController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VoicepathApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoicepathApplication.class, args);
	}


}
