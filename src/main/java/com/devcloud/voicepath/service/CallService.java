package com.devcloud.voicepath.service;

import com.devcloud.voicepath.model.TwilioCall;
import com.devcloud.voicepath.model.VoicePathCall;
import com.devcloud.voicepath.repository.VoicePathCallDAO;
import com.twilio.Twilio;
import com.twilio.http.HttpMethod;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;

@Service
public class CallService {
    private static final String twilioGeneralWebhook = "https://handler.twilio.com/twiml/EH66d643b1b9e553b731f4709d51623e5f";
    private static final String failureVoiceUrl = "https://handler.twilio.com/twiml/EH0dc780e4c0623f0f51784b20b4feb7c3";
    private String fromNumber = "+19739658330";
    private String token;
    private String sid;
//    private String webhookURI = "http://ec86bb1b.ngrok.io/webhook";
//    private String webhookURI = "http://b094b2ab.ngrok.io/webhook";
//    private String webhookURI = "http://5c441c07.ngrok.io/webhook";
    private String webhookURI = "https://fe59b0bc.ngrok.io/webhook";
    private PhoneNumber phoneNumberDialing = new PhoneNumber(fromNumber);
    private PhoneNumber phoneNumberDialed;

    @Autowired
    private VoicePathCallDAO voicePathCallDAO;


    public CallService(@Value("${TWILIO_SID}") String sid, @Value("${TWILIO_TOKEN}")
            String token) {
        this.sid = sid;
        this.token = token;
        Twilio.init(sid, token);

    }

    public Call makeCall(String toNumber) {

        phoneNumberDialed = new PhoneNumber(toNumber);
        Call call = Call.creator(phoneNumberDialed, phoneNumberDialing, URI.create(twilioGeneralWebhook))
                .setMethod(HttpMethod.GET)
                .setStatusCallback(URI.create(webhookURI))
                .setStatusCallbackEvent(Arrays.asList("initiated", "ringing", "answered", "no-answer", "completed"))
                .setStatusCallbackMethod(HttpMethod.POST)
                .create();
        return call;
    }

    public Call makeCallForFailure(String toNumber) {

        phoneNumberDialed = new PhoneNumber(toNumber);
        Call call = Call.creator(phoneNumberDialed, phoneNumberDialing, URI.create(failureVoiceUrl))
                .setMethod(HttpMethod.GET)
                .create();
        return call;
    }


    public Message sendMessage(String toNumber) {
        phoneNumberDialed = new PhoneNumber(toNumber);
        Message message = Message.creator(phoneNumberDialed, phoneNumberDialing, "A failure has been detected in the " +
                "following phone number: " + toNumber + '\n' + "The phone number belongs to the Princeton site.").create();

        return message;
    }

    /*public void getStatus() {
    }
*/
    public VoicePathCall twilioCallToVoicePathCall(Call twilioCall) {
        VoicePathCall voicePathCall = new VoicePathCall();

        voicePathCall.setTwilioSid(twilioCall.getSid());
        voicePathCall.setToNumber(twilioCall.getTo());
        voicePathCall.setFromNumber(fromNumber);
        voicePathCall.setCallStatus(twilioCall.getStatus().toString());
        voicePathCall.setDirection(twilioCall.getDirection());
        voicePathCall.setStartTime(LocalDateTime.now().toString());

        return voicePathCall;

    }

    public VoicePathCall twilioCallWebHookToVoicePathCall(TwilioCall twilioCall) {
        VoicePathCall voicePathCall = new VoicePathCall();

        voicePathCall.setTwilioSid(twilioCall.getCallSid());
        voicePathCall.setToNumber(twilioCall.getTo());
        voicePathCall.setFromNumber(twilioCall.getFrom());
        voicePathCall.setCallStatus(twilioCall.getCallStatus());
        voicePathCall.setDirection(twilioCall.getDirection());
        voicePathCall.setEndTime(LocalDateTime.now().toString());
        return voicePathCall;

    }

    public void saveVoicePathCall(VoicePathCall voicePathCall) {
        voicePathCallDAO.save(voicePathCall);
    }

    public VoicePathCall updateVoicePathCall(VoicePathCall voicePathCallFromTwilio) {

        VoicePathCall voicePathCall = voicePathCallDAO.findByTwilioSid(voicePathCallFromTwilio.getTwilioSid());
        voicePathCall.setCallStatus(voicePathCallFromTwilio.getCallStatus());
        voicePathCall.setEndTime(voicePathCallFromTwilio.getEndTime());
        voicePathCallDAO.save(voicePathCall);
        return voicePathCall;

    }

}
