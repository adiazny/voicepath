package com.devcloud.voicepath.repository;

import com.devcloud.voicepath.model.VoicePathCall;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoicePathCallDAO extends JpaRepository<VoicePathCall,Long> {

    VoicePathCall findByTwilioSid(String twilioSid);


}//interface
