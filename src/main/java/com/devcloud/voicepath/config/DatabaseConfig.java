package com.devcloud.voicepath.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories("com.devcloud.voicepath.repository")
@EnableTransactionManagement
public class DatabaseConfig {

}//class
